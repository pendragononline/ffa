var $ = function (id) { return document.getElementById(id); };
var navButton = document.getElementsByClassName("nav-button");
var noOfNavButtons = navButton.length;
var i = "";
var scrollDown = function () {
    var y = window.scrollY;
    if (y >= 300) {
        $('nav').classList.add("sticky-nav");
        $('search-field').classList.add("white-text");
        $('search-button').classList.add("white-text");
        for (i=0; i < noOfNavButtons; i++) {
            navButton[i].classList.add("white-text");
        } 
    }
    else {
        $('nav').classList.remove("sticky-nav");
        $('search-field').classList.remove("white-text");
        $('search-button').classList.remove("white-text");
        for (i=0; i < noOfNavButtons; i++) {
            navButton[i].classList.remove("white-text");
        } 
    }
};
window.addEventListener("scroll", scrollDown);
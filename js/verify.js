var $ = function (id) { return document.getElementById(id); };

var cardNumber = "";
var cvcNumber = "";
var emailAddress = "";

/*REAL-TIME CHECKING*/
function realtimeCheckName() {
    var elementId = "name";
    var elementName = "Name";
    generateRealtimeMessage(elementId, elementName);
}

function realtimeCheckAddress() {
    var elementId = "address";
    var elementName = "House and street name";
    generateRealtimeMessage(elementId, elementName);
}

function realtimeCheckPostcode() {
    var elementId = "postcode";
    var elementName = "Postcode";
    generateRealtimeMessage(elementId, elementName);
}

function realtimeCheckHolderName() {
    var elementId = "holder-name";
    var elementName = "Card holder name";
    generateRealtimeMessage(elementId, elementName);
}

function realtimeCheckCardNumber() {
    var elementId = "card-number";
    var elementName = "Card number";
    checkCardLength(elementId, elementName);
    generateRealtimeMessage(elementId, elementName);
}

function realtimeCheckCvc() {
    var elementId = "cvc";
    var elementName = "CVC";
    checkCvcLength(elementId, elementName);
    generateRealtimeMessage(elementId, elementName);
}

function realtimeCheckEmailAddress() {
    var elementId = "email-address";
    var elementName = "E-mail address";
    checkEmailAddress(elementId, elementName);
    generateRealtimeMessage(elementId, elementName);
}

function realtimeCheckTelNumber() {
    var elementId = "tel-number";
    var elementName = "Telephone number";
    generateRealtimeMessage(elementId, elementName);
}

function checkCardLength(elementId, elementName) {
    cardNumber = $('card-number').value.toString();
    if (cardNumber.length !== 16) {
        console.log(cardNumber.length);
        $('required-notice-' + elementId).innerHTML = elementName + " must be 16 digits long.";
        showRealtimeMessage(elementId);
    }
}

function checkCvcLength(elementId, elementName) {
    cvcNumber = $('cvc').value.toString();
    if (cvcNumber.length !== 3) {
        $('required-notice-' + elementId).innerHTML = elementName + " must be 3 digits long.";
        showRealtimeMessage(elementId);
    }
}

function checkEmailAddress(elementId, elementName) {
    emailAddress = $('email-address').value;
    if (emailAddress.includes("@") === false || emailAddress.includes(".") === false || emailAddress.includes(" ") === true) {
        $('required-notice-' + elementId).innerHTML = elementName + " must contain an @ symbol, a full stop and no spaces.";
        showRealtimeMessage(elementId);
    }
}

function generateRealtimeMessage(elementId, elementName) {
    if ($(elementId).value === "") {
        $('required-notice-' + elementId).innerHTML = elementName + " is a required field.";
        showRealtimeMessage(elementId);
    }
}

function showRealtimeMessage(elementId) {
    $('required-notice-' + elementId).classList.remove('hidden');
    $('required-notice-' + elementId).setAttribute('aria-hidden', 'false');
    $(elementId).classList.add("field-error");
}

function hideRealtimeMessage(element) {
    if ($(element).value !== "") {
        $('required-notice-' + element).classList.add('hidden');
        $('required-notice-' + element).setAttribute('aria-hidden', 'true');
        $(element).classList.remove("field-error");
    }
}

/*PURCHASE BUTTON CHECKING - theoretically never activated if all fields completed in order*/
function checkDetails() {
    var errorHtmlId = document.getElementById("error-type");
    checkItems(errorHtmlId);
}

function showErrorPanel() {
    $('errors-found').setAttribute('aria-hidden', 'false');
    $('errors-found').classList.remove('offscreen');
}

function checkItems(errorHtmlId) {
    var elementId = "";
    var errorStatus = "";
    var errorMessage = "";
    cardNumber = $('card-number').value.toString();
    cvcNumber = $('cvc').value.toString();
    emailAddress = $('email-address').value;
    if ($('name').value === "") {
        errorMessage = "Please provide a name in your delivery address.";
        errorStatus = 1;
        elementId = "name";
    }
    if ($('address').value === "") {
        errorMessage = "Please provide a house number and street name to deliver to.";
        errorStatus = 1;
        elementId = "address";
    }
    if ($('postcode').value === "") {
        errorMessage = "Please provide a post code in your delivery address.";
        errorStatus = 1;
        elementId = "postcode";
    }
    if ($('holder-name').value === "") {
        errorMessage = "Please provide a cardholder name.";
        errorStatus = 1;
        elementId = "holder-name";
    }
    if ($('card-number').value === "" || cardNumber.length !== 16) {
        errorMessage = "Please provide a valid 16 digit card number.";
        errorStatus = 1;
        elementId = "card-number";
    }
    if ($('cvc').value === "" || cvcNumber.length !== 3) {
        errorMessage = "Please provide a valid 3 digit CVC number.";
        errorStatus = 1;
        elementId = "cvc";
    }
    if ($('email-address').value === "" || emailAddress.includes("@") === false || emailAddress.includes(".") === false || emailAddress.includes(" ") === true) {
        errorMessage = "Please provide a valid email address.";
        errorStatus = 1;
        elementId = "email-address";
    }
    if ($('tel-number').value === "") {
        errorMessage = "Please provide a valid telephone number.";
        errorStatus = 1;
        elementId = "tel-number";
    }

    if (errorStatus === 1) {
        showErrorPanel();
        errorHtmlId.innerHTML = errorMessage;
        var rectifyButton = document.getElementById("correct-errors-button");
        rectifyButton.addEventListener("click", function () {
            document.getElementById(elementId).classList.add("field-error");
            document.getElementById(elementId).focus();
            window.location.href = "#" + elementId;
        });
    }
    else {
        window.location.href = "http://pendragon.online/demo/ffa/success.html";
    }
}
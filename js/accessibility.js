var $ = function (id) { return document.getElementById(id); };

var pElements = document.getElementsByTagName('p');
var h1Elements = document.getElementsByTagName('h1');
var h2Elements = document.getElementsByTagName('h2');
var h3Elements = document.getElementsByTagName('h3');
var legendElements = document.getElementsByTagName('legend');
var labelElements = document.getElementsByTagName('label');
var liElements = document.getElementsByTagName('li');
var selectElements = document.getElementsByTagName('select');
var inputElements = document.getElementsByTagName('input');
var headerElement = document.getElementsByTagName('header');
var footerElement = document.getElementsByTagName('footer');
var buttons = document.getElementsByClassName('button');
var stickynav = document.getElementsByClassName('sticky-nav');
var menuButtons = document.getElementsByClassName('hamburger-button-style');
var mobileButtons = document.getElementsByClassName('mobile-buttons');
var secondLayerNav = document.getElementsByClassName('nav-layer-2');
var links = document.getElementsByClassName('link');
var svgImg = document.getElementsByClassName("cls-1");
var columnElements = document.getElementsByClassName("column");
var containerElements = document.getElementsByClassName("container");
var mobileSearchIcon = $('search-icon-menu');

var colours = [
    "#EBEBEB", //default background
    "white", //white
    "black", //default text
    "#dafa06", //yellow
    "#0439cf", //blue
    "#41f60a", //green
    "#c40303" //red
];

function hideFooterLogo() {
    $('col1').style.display = "none";
}

function showFooterLogo() {
    $('col1').style.display = "block";
}

function applyDefaultSize() {
    defaultColumns();
    removePreviousFontSetting();
    localStorage.removeItem("fontsize");
    showFooterLogo();
}

function applyMediumSize() {
    var fontSizeOption = "med";
    wideColumns();
    hideSecondLayerNav();
    removePreviousFontSetting();
    applyFontSize(fontSizeOption);
    hideFooterLogo();
}

function applyLargeSize() {
    var fontSizeOption = "large";
    wideColumns();
    hideSecondLayerNav();
    removePreviousFontSetting();
    applyFontSize(fontSizeOption);
    hideFooterLogo();    
}

function applyFontSize(fontSizeOption) {
    for (i = 0; i < pElements.length; i++) {
        pElements[i].classList.add("p-" + fontSizeOption);
    }
    for (i = 0; i < buttons.length; i++) {
        buttons[i].classList.add("p-" + fontSizeOption);
    }
    for (i = 0; i < h1Elements.length; i++) {
        h1Elements[i].classList.add("h1-" + fontSizeOption);
    }
    for (i = 0; i < h2Elements.length; i++) {
        h2Elements[i].classList.add("h2-" + fontSizeOption);
    }
    for (i = 0; i < h3Elements.length; i++) {
        h3Elements[i].classList.add("h3-" + fontSizeOption);
    }
    for (i = 0; i < legendElements.length; i++) {
        legendElements[i].classList.add("p-" + fontSizeOption);
    }
    for (i = 0; i < labelElements.length; i++) {
        labelElements[i].classList.add("p-" + fontSizeOption);
    }
    for (i = 0; i < liElements.length; i++) {
        liElements[i].classList.add("p-" + fontSizeOption);
    }
    for (i = 0; i < selectElements.length; i++) {
        selectElements[i].classList.add("p-" + fontSizeOption);
    }
    for (i = 0; i < inputElements.length; i++) {
        inputElements[i].classList.add("p-" + fontSizeOption);
    }
    for (i = 0; i < menuButtons.length; i++) {
        menuButtons[i].classList.add("p-" + fontSizeOption);
    }
    for (i = 0; i < inputElements.length; i++) {
        inputElements[i].classList.add("p-" + fontSizeOption);
        inputElements[i].classList.add("input-" + fontSizeOption);
    }
    for (i = 0; i < mobileButtons.length; i++) {
        mobileButtons[i].classList.add("mobile-button-placement-" + fontSizeOption);
    }
    mobileSearchIcon.classList.add("search-icon-" + fontSizeOption);
    saveFontSetting(fontSizeOption);
}

function removePreviousFontSetting() {
    var previousSetting = localStorage.getItem("fontsize");
    for (i = 0; i < pElements.length; i++) {
        pElements[i].classList.remove("p-" + previousSetting);
    }
    for (i = 0; i < buttons.length; i++) {
        buttons[i].classList.remove("p-" + previousSetting);
    }
    for (i = 0; i < h1Elements.length; i++) {
        h1Elements[i].classList.remove("h1-" + previousSetting);
    }
    for (i = 0; i < h2Elements.length; i++) {
        h2Elements[i].classList.remove("h2-" + previousSetting);
    }
    for (i = 0; i < h3Elements.length; i++) {
        h3Elements[i].classList.remove("h3-" + previousSetting);
    }
    for (i = 0; i < legendElements.length; i++) {
        legendElements[i].classList.remove("p-" + previousSetting);
    }
    for (i = 0; i < labelElements.length; i++) {
        labelElements[i].classList.remove("p-" + previousSetting);
    }
    for (i = 0; i < liElements.length; i++) {
        liElements[i].classList.remove("p-" + previousSetting);
    }
    for (i = 0; i < selectElements.length; i++) {
        selectElements[i].classList.remove("p-" + previousSetting);
    }
    for (i = 0; i < inputElements.length; i++) {
        inputElements[i].classList.remove("p-" + previousSetting);
    }
    for (i = 0; i < menuButtons.length; i++) {
        menuButtons[i].classList.remove("p-" + previousSetting);
    }
    for (i = 0; i < inputElements.length; i++) {
        inputElements[i].classList.add("p-" + previousSetting);
        inputElements[i].classList.remove("input-" + previousSetting);
    }
    for (i = 0; i < mobileButtons.length; i++) {
        mobileButtons[i].classList.add("mobile-button-placement-" + previousSetting);
    }
    mobileSearchIcon.classList.add("search-icon-" + previousSetting);
    localStorage.removeItem("fontsize");
}

function wideColumns() {
    for (i = 0; i < columnElements.length; i++) {
        columnElements[i].classList.add("wide-col");
        columnElements[i].classList.remove("centre");
    }
}

function defaultColumns() {
    for (i = 0; i < columnElements.length; i++) {
        columnElements[i].classList.remove("wide-col");
        //columnElements[i].classList.add("centre");
    }
}

function hideSecondLayerNav() {
    for (i = 0; i < secondLayerNav.length; i++) {
        secondLayerNav[i].classList.add("hidden");
    }
}

function defaultTextColour() {
    var textColourOption = "default";
    var textColour = colours[2];
    applyTextColour(textColour, textColourOption);
    localStorage.removeItem("textcolour");
}

function whiteTextColour() {
    var textColourOption = "white";
    var textColour = colours[1];
    applyTextColour(textColour, textColourOption);
}

function blackTextColour() {
    var textColourOption = "black";
    var textColour = colours[2];
    applyTextColour(textColour, textColourOption);
}

function yellowTextColour() {
    var textColourOption = "yellow";
    var textColour = colours[3];
    applyTextColour(textColour, textColourOption);
}

function blueTextColour() {
    var textColourOption = "blue";
    var textColour = colours[4];
    applyTextColour(textColour, textColourOption);
}

function greenTextColour() {
    var textColourOption = "green";
    var textColour = colours[5];
    applyTextColour(textColour, textColourOption);
}

function redTextColour() {
    var textColourOption = "red";
    var textColour = colours[6];
    applyTextColour(textColour, textColourOption);   
}

function applyTextColour(textColour, textColourOption) {
    for (i = 0; i < pElements.length; i++) {
        pElements[i].style.color = textColour;
    }
    for (i = 0; i < buttons.length; i++) {
        buttons[i].style.color = textColour;
    }
    for (i = 0; i < h1Elements.length; i++) {
        h1Elements[i].style.color = textColour;
    }
    for (i = 0; i < h2Elements.length; i++) {
        h2Elements[i].style.color = textColour;
    }
    for (i = 0; i < h3Elements.length; i++) {
        h3Elements[i].style.color = textColour;
    }
    for (i = 0; i < legendElements.length; i++) {
        legendElements[i].style.color = textColour;
    }
    for (i = 0; i < labelElements.length; i++) {
        labelElements[i].style.color = textColour;
    }
    for (i = 0; i < liElements.length; i++) {
        liElements[i].style.color = textColour;
    }
    for (i = 0; i < selectElements.length; i++) {
        selectElements[i].style.color = textColour;
    }
    for (i = 0; i < inputElements.length; i++) {
        inputElements[i].style.color = textColour;
    }
    highContrastHeaderFooter();
    setSpecialElementColours(textColour);
    saveTextSetting(textColourOption);
}

function highContrastHeaderFooter() {
    for (i = 0; i < headerElement.length; i++) {
        headerElement[i].style.background = colours[2];
    }
    for (i = 0; i < footerElement.length; i++) {
        footerElement[i].style.background = colours[2];
    }
    for (i = 0; i < stickynav.length; i++) {
        stickynav[i].style.background = colours[2];
    }
}

function resetHeaderFooter() {
    for (i = 0; i < headerElement.length; i++) {
    headerElement[i].style.background = "#1F1F1F";
    }
    for (i = 0; i < footerElement.length; i++) {
        footerElement[i].style.background = "#1F1F1F";
    }
    for (i = 0; i < stickynav.length; i++) {
        stickynav[i].style.background = "#222222";
    }
}

function setSpecialElementColours(textColour) {
    for (i = 0; i < buttons.length; i++) {
        buttons[i].classList.add("bottom-margin");
    }
    if (textColour !== "black") {
        for (i = 0; i < buttons.length; i++) {
            buttons[i].style.outline = "1.2vh solid " + textColour;
            buttons[i].style.background = "black";
        }
        for (i = 0; i < selectElements.length; i++) {
            selectElements[i].style.background = "black";
        }
    }
    else {
        for (i = 0; i < buttons.length; i++) {
            buttons[i].style.outline = "1.2vh solid " + textColour;
            buttons[i].style.background = "white";
        }
        for (i = 0; i < selectElements.length; i++) {
            selectElements[i].style.background = "white";
            selectElements[i].style.outline = "1.2vh solid " + textColour;
            selectElements[i].style.margin = "2vh";
        }
    } 
}

function unsetSpecialElementColours() {
    for (i = 0; i < buttons.length; i++) {
        buttons[i].classList.remove("bottom-margin");
    }
    for (i = 0; i < buttons.length; i++) {
        buttons[i].style.outline = "none";
        buttons[i].style.background = "#1F1F1F";
    }
    for (i = 0; i < selectElements.length; i++) {
        selectElements[i].style.background = "#1F1F1F";
        selectElements[i].style.outline = "none";
        selectElements[i].style.margin = "0vh";
        selectElements[i].style.color = "white";
    }
}

function defaultBackground() {
    var bgColourOption = "default";
    resetHeaderFooter();
    unsetSpecialElementColours();
    saveBackgroundSetting(bgColourOption);
    document.body.style.background = colours[0];
    for (i = 0; i < buttons.length; i++) {
        buttons[i].style.background = "#1F1F1F";
        buttons[i].style.color = "white";
    }
    localStorage.removeItem("bgcolour");
}

function whiteBackground() {
    var bgColourOption = "white";
    saveBackgroundSetting(bgColourOption);
    document.body.style.background = colours[1];
}

function blackBackground() {
    var bgColourOption = "black";
    saveBackgroundSetting(bgColourOption);
    document.body.style.background = colours[2];
}

function yellowBackground() {
    var bgColourOption = "yellow";
    saveBackgroundSetting(bgColourOption);
    document.body.style.background = colours[3];
}

function blueBackground() {
    var bgColourOption = "blue";
    saveBackgroundSetting(bgColourOption);
    document.body.style.background = colours[4];
}

function greenBackground() {
    var bgColourOption = "green";
    saveBackgroundSetting(bgColourOption);
    document.body.style.background = colours[5];
}

function redBackground() {
    var bgColourOption = "red";
    saveBackgroundSetting(bgColourOption);
    document.body.style.background = colours[6];
}

function saveFontSetting(fontSizeOption) {
    localStorage.setItem("fontsize", fontSizeOption); 
} 

function saveTextSetting(textColourOption) {
    localStorage.setItem("textcolour", textColourOption); 
}

function saveBackgroundSetting(bgColourOption) {
    localStorage.setItem("bgcolour", bgColourOption);
}

function applyUserSettings() {
    //Font size
    if (localStorage.getItem("fontsize") === null || localStorage.getItem("fontsize") === "default") {
        applyDefaultSize();
    }
    else if (localStorage.getItem("fontsize") === "med") {
        applyMediumSize();
    }
    else {
        applyLargeSize();
    }
    //Font colour
    if (localStorage.getItem("textcolour") === null || localStorage.getItem("textcolour") === "default") {
        defaultTextColour();
    }
    else if (localStorage.getItem("textcolour") === "white") {
        whiteTextColour();
    }
    else if (localStorage.getItem("textcolour") === "black") {
        blackTextColour();
    }
    else if (localStorage.getItem("textcolour") === "yellow") {
        yellowTextColour();
    }
    else if (localStorage.getItem("textcolour") === "blue") {
        blueTextColour();
    }
    else if (localStorage.getItem("textcolour") === "green") {
        greenTextColour();
    }
    else {
        redTextColour();
    }
    //Background colour
    if (localStorage.getItem("bgcolour") === null || localStorage.getItem("bgcolour") === "default") {
        defaultBackground();
    }
    else if (localStorage.getItem("bgcolour") === "white") {
        whiteBackground();
    }
    else if (localStorage.getItem("bgcolour") === "black") {
        blackBackground();
    }
    else if (localStorage.getItem("bgcolour") === "yellow") {
        yellowBackground();
    }
    else if (localStorage.getItem("bgcolour") === "blue") {
        blueBackground();
    }
    else if (localStorage.getItem("bgcolour") === "green") {
        greenBackground();
    }
    else {
        redBackground();
    }
}

applyUserSettings(); //run on page load
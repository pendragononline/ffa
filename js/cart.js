var $ = function (id) { return document.getElementById(id); };

var id = 1;
var currentPage = window.location.href;

var totalCost = parseFloat(localStorage.getItem("price")); //initially set the total cost as the item price (as a float)

function goHome() {
    window.href = "http://pendragon.online/demo/ffa/index.html";
}

function featureNotAvailable() {
    window.alert("This feature is not available in this prototype.");
}

function addItem() {
    createProductObject();
    $('added-to-cart').setAttribute('aria-hidden', 'false');
    $('buy-buttons').setAttribute('aria-hidden', 'false');
    $('item-name').innerHTML = productName.innerHTML + " has been added to your cart"; 
    $('item-size').innerHTML = $("select-size").value;
    $('added-to-cart').classList.remove("offscreen");
}

function createProductObject() {
    var productObject = {};
    productObject.id = id++;
    productObject.name = $('productName').innerHTML;
    productObject.price = $('productPrice').innerHTML;
    productObject.size = $("select-size").value;
    productObject.url = producturl;
    productObject.imageurl = imageurl;
    productObject.nextgenurl = nextgenurl;
    saveProduct(productObject);
}

function saveProduct(productObject) {
    var productName = productObject.name;
    var productPrice = productObject.price;
    var productSize = productObject.size;
    var productURL = productObject.url;
    var productImage = productObject.imageurl;
    var productNextGen = productObject.nextgenurl;
    localStorage.setItem("name", productName);
    localStorage.setItem("price", productPrice);
    localStorage.setItem("size", productSize);
    localStorage.setItem("url", productURL);
    localStorage.setItem("image", productImage);
    localStorage.setItem("nextgen", productNextGen);
    updateCart(productObject);
}

function updateCart(productObject) {
    updateCartNumber();
    if (currentPage.includes('cart.html')) {
        showItemsInCart(productObject);
    }
}

function updateCartNumber() {
    if (localStorage.getItem("name") === null) {
        $('cart-button').innerHTML = "CART";
    }
    else {
        $('cart-button').innerHTML = "CART (1)";
    } 
}

function showItemsInCart() {
    if (localStorage.getItem("name") === null) {
        $('product-title-cart').innerHTML = "Your cart is empty";
    }
    else {
        $('product-information-cart').classList.remove("hidden");
        $('product-price-info-cart').classList.remove("hidden");
        $('product-size-info-cart').classList.remove("hidden");
        $('buy-item-cart').classList.remove("hidden");
        $('view-item-cart').classList.remove("hidden");
        $('remove-item-cart').classList.remove("hidden");
        $('empty-cart-button').classList.remove("hidden");
        $('product-title-cart').innerHTML = localStorage.getItem("name");
        $('product-price-cart').innerHTML = localStorage.getItem("price");
        $('product-size-cart').innerHTML = localStorage.getItem("size");
    }   
}

function viewProduct() {
    var productUrl = localStorage.getItem("url");
    window.href.location = productUrl;
}

function closeAddedToCart() {
    $('added-to-cart').classList.add("offscreen");
    $('added-to-cart').setAttribute('aria-hidden', 'true');
}

function clearCartText() {
    $('product-information-cart').classList.add("hidden");
    $('product-price-cart').classList.add("hidden");
    $('product-size-info-cart').classList.add("hidden");
    $('view-item-cart').classList.add("hidden");
    $('buy-item-cart').classList.add("hidden");
    $('remove-item-cart').classList.add("hidden");
    $('empty-cart-button').classList.add("hidden");
}

function emptyCart() {
    localStorage.removeItem("name");
    localStorage.removeItem("price");
    localStorage.removeItem("size");
    localStorage.removeItem("url");
    localStorage.removeItem("image");
    localStorage.removeItem("nextgen");
    $('product-price-info-cart').classList.add('hidden');
    clearCartText();
    showItemsInCart();
}

function calculateDelivery() {
    var weekday = new Array(7); weekday[0] = "Sunday"; weekday[1] = "Monday"; weekday[2] = "Tuesday"; weekday[3] = "Wednesday"; weekday[4] = "Thursday"; weekday[5] = "Friday"; weekday[6] = "Saturday";
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var months30 = ['April', 'June', 'September', 'November'];
    var months31 = ['January', 'March', 'May', 'July', 'August', 'October'];
    var today = new Date();
    var currentMonth = months[today.getMonth()];
    var currentDay = weekday[today.getDay() + 1];
    var postagePrice = "";
    var deliveryDay = parseInt(today.getDate(), 10);
    /*Go back to Sunday at end of week*/
    if (today.getDay() === 6) {
        currentDay = weekday[0];
    }
    /*Set new month and 1st at the end of current month*/
    if (deliveryDay >= 28 && currentMonth === "February") {
        deliveryDay = 0;
        daysInMonth = 28;
        currentMonth = months[today.getMonth() +1];
    }
    if (deliveryDay >= 30 && months30.includes(currentMonth)) {
        deliveryDay = 0;
        currentMonth = months[today.getMonth() + 1];
    }
    if (deliveryDay >= 31 && months31.includes(currentMonth)) {
        deliveryDay = 0;
        currentMonth = months[today.getMonth() + 1];
    }
    /*Go to January at the end of the year*/
    if (currentMonth === "December" && deliveryDay >= 31) {
        deliveryDay = 0;
        currentMonth = months[0];
    }
    /*Calculate and show costs and delivery times*/
    $('estimated-delivery-date').classList.remove("hidden");
    if ($('special-delivery').checked === true) {
        deliveryDay = deliveryDay + 1;
        deliveryDate = "on " + currentDay + " " + deliveryDay + " " + currentMonth;
        postageType = "Special Delivery (Next day)";
        postagePrice = 7.49;
    }
    else {
        deliveryDay = deliveryDay + 7;
        deliveryDate = "on or before " + currentDay + " " + deliveryDay + " " + currentMonth;
        postageType = "Second Class Delivery (7 days)";
        postagePrice = 3.49;
    }
    $('delivery-date-estimate').innerHTML = deliveryDate;
    calculateCost(postagePrice, postageType, deliveryDate);
}

function calculateCost(postagePrice, postageType, deliveryDate) {
    $('postage-price-checkout-number').innerHTML = postagePrice;
    $('postage-estimate-checkout').innerHTML = "Delivery estimate: " + deliveryDate + " (" + postageType + ")";
    $('postage-price-checkout').classList.remove("hidden");
    $('postage-estimate-checkout').classList.remove("hidden");
    totalCost = Math.round((totalCost + postagePrice)*100)/100;
    showUpdatedTotalPrice(totalCost);
}

function showUpdatedTotalPrice() {
    $('product-total-price-checkout').innerHTML = totalCost;
}

function showCheckoutDetails() {
    $('product-title').innerHTML = localStorage.getItem("name");
    $('product-size').innerHTML = localStorage.getItem("size");
    $('product-price-checkout').innerHTML = localStorage.getItem("price");
    $('product-total-price-checkout').innerHTML = totalCost;
}

function readBackDetails() {
    var paymentType = "";
    var deliveryType = "";
    var nameError = "";
    var addressError = "";
    var cardError = "";
    var cvcError = "";
    var emailError = "";
    var postcodeError = "";
    var holderError = "";
    var telError = "";
    var errorState = "";
    var errorAnnounce = "";
    var errorButtonAnnounce = "";
    var firstError = 0;
    cardNumber = $('card-number').value.toString();
    cvcNumber = $('cvc').value.toString();
    emailAddress = $('email-address').value;
    if ($('debit').checked === true) {
        paymentType = $('debit').value;
    }
    else if ($('credit').checked === true) {
        paymentType = $('credit').value;
    }
    else if ($('electron').checked === true) {
        paymentType = $('electron').value;
    }
    else {
        paymentType = $('mastercard').value;
    }
    if ($('special-delivery').checked === true) {
        deliveryType = $('special-delivery').value;
    }
    else {
        deliveryType = $('second-class').value;
    }
    if ($('name').value === "") {
        nameError = "Blank. Name is a required field. Please complete before checkout. ";
        errorState = 1;
        errorId = "name";
        checkIfFirst(firstError, errorId);
    }
    if ($('address').value === "") {
        addressError = "Blank. House and street name is a required field. Please complete before checkout. ";
        errorState = 1;
        errorId = "address";
        checkIfFirst(firstError, errorId);
    }
    if ($('postcode').value === "" ) {
        postcodeError = "Blank. Postcode is a required field. Please complete before checkout. ";
        errorState = 1;
        errorId = "postcode";
        checkIfFirst(firstError, errorId);
    }
    if ($('holder-name').value === "") {
        holderError = "Blank. Card holder name is a required field. Please complete before checkout. ";
        errorState = 1;
        errorId = "holder-name";
        checkIfFirst(firstError, errorId);
    }
    if ($('tel-number').value === "") {
        telError = "Blank. Telephone number is a required field. Please complete before checkout. ";
        errorState = 1;
        errorId = "tel-number";
        checkIfFirst(firstError, errorId);
    }
    if ($('card-number') === "" || cardNumber.length !== 16) {
        cardError = "In valid. The card number must be present, and it must be 16 digits long. Please correct before checkout. "
        errorState = 1;
        errorId = "card-number";
        checkIfFirst(firstError, errorId);
    }
    if ($('cvc').value === "" || cvcNumber.length !== 3) {
        cvcError = "In valid. The CVC number must be present, and it must be 3 digits long. Please correct before checkout. ";
        errorState = 1;
        errorId = "cvc";
        checkIfFirst(firstError, errorId);
    }
    if ($('email-address').value === "" || emailAddress.includes("@") === false || emailAddress.includes(".") === false || emailAddress.includes(" ") === true) {
        emailError = "In valid. An e-mail address must be present, and it must contain at least one full stop, an @ symbol and no spaces. Please correct before checkout. ";
        errorState = 1;
        errorId = "email-address";
        checkIfFirst(firstError, errorId);
    }
    if (errorState === 1) {
        errorAnnounce = "There were errors. Please listen carefully. ";
        errorButtonAnnounce = " Swipe or key twice to correct these errors. "
        $('correctErrorsReadback').setAttribute('aria-hidden', 'false');
    }
    $('customer-details').innerHTML =
        errorAnnounce + 
        "Payment type: " + paymentType +
        ". \nDelivery type: " + deliveryType +
        ". \nBilling Address Name: " + nameError + $('name').value +
        ". \nBilling Address House and Street: " + addressError + $('address').value +
        ". \nBilling Address Post Code: " + postcodeError + $('postcode').value +
        ". \nBilling Address Country: " + $('country').value +
        ". \nCard holder name: " + holderError + $('holder-name').value +
        ". \nCard number: " + cardError + $('card-number').value +
        ". \nCard CVC: " + cvcError + $('cvc').value +
        ". \nContact e-mail address: " + emailError + $('email-address').value +
        ". \nContact telephone number: " + telError + $('tel-number').value +
        errorButtonAnnounce;
    disableReadbackMsg();
}

function checkIfFirst(firstError, errorId) {
    console.log(errorId, firstError);
    if (firstError === 0) {
        firstError = errorId;
    }
}

function topOfCheckout(errorId) {
    window.location.href = "#"+errorId;
    $(errorId).focus();
    $(errodId).setAttribute('aria-live', 'assertive');
}

function completePurchase() {
    $('product-name-complete').innerHTML = localStorage.getItem("name");
    emptyCart();
}

//Run on cart page only
if (currentPage.includes('cart.html')) {
    showItemsInCart();
}

//Run on checkout page only
if (currentPage.includes('checkout.html')) {
    showCheckoutDetails();
    calculateDelivery();
}

//Run on successful purchase page only
if (currentPage.includes('success.html')) {
    completePurchase();
}

//Run on all pages
updateCartNumber();